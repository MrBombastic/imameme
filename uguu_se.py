import os, subprocess, pycurl, six, config


if six.PY2:
    from StringIO import StringIO
elif six.PY3:
    from io import BytesIO

from uploader import uploader

class uguu_se(uploader):

    def __init__(self):
        super(uguu_se, self).__init__("uguu_se", 100*1000*1000, 0)

    def upload(self, file):
        output = None
        if six.PY2:
            output = StringIO()
        elif six.PY3:
            output = BytesIO()
        print("File: '"+file+"'")

        c = pycurl.Curl()
        c.setopt(c.URL, 'https://uguu.se/api.php?d=upload-tool')
        c.setopt(c.HTTPPOST, [("name", (c.FORM_FILE, file))])
        c.setopt(c.HTTPPOST, [("file", (c.FORM_FILE, file))])
        
        if six.PY2:
            c.setopt(c.WRITEFUNCTION, output.write)
        elif six.PY3:
            c.setopt(c.WRITEFUNCTION, output.write)
        c.setopt(pycurl.SSL_VERIFYPEER, config.use_ssl) 

        c.perform()
        c.close()

        value = None
        
        if six.PY2:
            value = output.getvalue()
        elif six.PY3:
            value = output.getvalue().decode('utf-8')
        return value