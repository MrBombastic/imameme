import config, renameImage, os

if config.active == False:
    print("Please check your config settings and set active to 'True'")
    quit()

original_dir = os.getcwd()
main_dir = os.path.dirname(os.path.realpath(__file__))

os.chdir(main_dir)
print("Working dir: "+os.getcwd())

renameImage.start()

os.chdir(original_dir)