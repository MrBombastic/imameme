import sys, os, ntpath, time, random, subprocess, datetime, config, shutil, six, dictlogging, calendar

if six.PY2:
    from urlparse import urljoin
elif six.PY3:
    #from urllib.parse import urljoin
    from urllib.parse import urljoin

from guessImageName import getGuess

def file_move_safe(source, target, index=-1):
    path = target + ""
    if index!=-1:
        onlyfile = os.path.basename(target)
        filename, file_extension = os.path.splitext(onlyfile)
        parent = os.path.abspath(os.path.join(target, os.pardir))
        nfn = filename + " (" + ("%d" % index) + ")" + file_extension
        path = os.path.join(parent, nfn)

    if os.path.exists(path):
        index = index + 1
        file_move_safe(source,target,index)
    else:
        os.rename(source,path)

def readWords():
    text_file = None
    if six.PY2:
        text_file = open(os.path.abspath(config.wordlist), "r")    
    elif six.PY3:
        text_file = open(os.path.abspath(config.wordlist), "r", errors='replace')
    lines = text_file.readlines()
    wordlists = lines
    string = "Wordlist entries: %d" % len(wordlists)
    print(string)
    return wordlists

def isImage(file):
    for ext in config.image_file_extensions:
        if file.endswith(ext):
            return True
    return False

def listFiles(source):
    files = []
    s = os.path.abspath(source)
    # r=root, d=directories, f = files
    for r, d, f in os.walk(s):
        for file in f:
            if isImage(file):
                files.append(os.path.join(r, file))
    return files

def isValid(wordscore, length):
    score = (100*wordscore/((1+length)**(1.41)))
    sc = "Score: %d   Avg: %d" % (wordscore, score)
    print(sc)
    if score >= 300:
        return True
    else:
        return False

def isWord(text, wordlist):
    wordscore = 0
    t = text[:-4]
    for word in wordlist:
        w = word.rstrip()
        if w in text:
            wordscore += len(w)*len(w)
    return isValid(wordscore, len(t))

def uploadImage(file):
    dictlogging.registerUpload(file)
    if config.uploader == None:
        shutil.copy(file, os.path.join(config.webserver_path, ntpath.basename(file)))
        path = None
        if six.PY2:
            path = urljoin(config.webserver_url,ntpath.basename(file))
        elif six.PY3:
            path = urljoin(config.webserver_url,ntpath.basename(file))
        return path
    else:
        return config.uploader.upload(file)

def organizeFile(amount_files, amount_empty_guesses, wordlist):
    amount_files+=1
    for file in listFiles(os.path.abspath(config.source)):
        print("")
        if config.uploader != None and config.uploader.canUpload(file) == False:
            return None
        f = ntpath.basename(file)
        n = "" + f
        keepname = False
        if isWord(f.lower(), wordlist):
            keepname = True
            for testw in config.ignorenames:
                if testw in f.lower():
                    keepname = False    
        web = uploadImage(file)
        print("Web: "+web)
        guess = getGuess(web)
        filename, file_extension = os.path.splitext(n)
        print("File: "+f+"   Guess: >"+guess+"<")
        if guess=="":
            amount_empty_guesses+=1
        if keepname:
            if guess != "":
                n = filename + " ("+guess+")" + file_extension
        else:
            if guess == "":
                print("No guess but name isnt good either...marking file with ___")
                n = "___" + n
            else:
               n = guess + file_extension
        tn = os.path.abspath(os.path.join(config.target,n))
        if config.uploader == None:
            os.remove(os.path.join(config.webserver_path, ntpath.basename(file)))
        file_move_safe(file,tn)
        return [file,tn]
    return []

def check(wordlist):
    amount_files = 0
    amount_empty_guesses = 0
    while True:
        dictlogging.updateLog()
        filepair = None
        while len(dictlogging.log) >= config.max_amount_of_requests_per_24h:
            timer = 30
            if dictlogging.nextToBeRemoved != None:
                timer = (24*60*60) - (calendar.timegm(time.gmtime()) - dictlogging.nextToBeRemoved) + 1
            print("Sleep "+str(timer)+" seconds, then another upload should be possible (limit of "+str(config.max_amount_of_requests_per_24h)+" requests per 24h reached)")
            time.sleep(timer)
        while filepair == None:
            filepair = organizeFile(amount_files,amount_empty_guesses, wordlist)
            if filepair == None:
                config.uploader.sleep()
                dictlogging.updateLog()
        if len(filepair) == 0:
            print("")
            print("No files in source left!")
            break
        print("Additional work...")
        start = calendar.timegm(time.gmtime())
        ttime = config.min_sleep_after_rename+random.randint(0,config.max_sleep_after_rename-config.min_sleep_after_rename)
        if config.after_rename_script != None:
            print("Execute command...")
            try:
                list = []
                list.append(config.after_rename_script)
                list.append(filepair[0])
                list.append(filepair[1])
                list2 = []
                for string in list:
                    list2.append(string.encode('iso-8859-1'))
                subprocess.check_output(list2)
            except:
                print("The command you specified is not working. Check this out.")
            print("Command execution complete")
        sl = ttime-(calendar.timegm(time.gmtime())-start)
        slt = "Sleeping for " + str(sl) + " seconds, then look for the next file."
        print(slt)
        time.sleep(max(sl,1))
        print("Waking up...")
    if amount_files != 0:
        print("Check done, no files left.")
        print("Files: %d" % amount_files)
        print("Empty guesses: %d" % amount_empty_guesses)
    amount_files = 0
    amount_empty_guesses = 0

def start():
    if os.path.exists(os.path.abspath(config.target)) == False:
        os.mkdir(os.path.abspath(config.target))
    wordlist = readWords()
    while True:
        check(wordlist)
        if config.keep_program_running == False:
            break
        print("Waiting for " + str(config.sleep_when_no_file_found) + " seconds, then look for new suitable source files.")
        time.sleep(config.sleep_when_no_file_found)

