import dictlogging, os, calendar, time, dictlogging
from hurry.filesize import size

class uploader(object):

    max_size_per_day = 0
    max_amount_of_files = 0

    nextsleep = 30

    def __init__(self, name, max_size_per_day, max_amount_of_files):
        self.name = name
        self.max_size_per_day = max_size_per_day
        self.max_amount_of_files = max_amount_of_files
        print("Uploader[\""+name+"\"]-settings: ")
        if self.max_size_per_day == 0:
            print("Max. size per day: Unlimited")
        else:
            print("Max. size per day: "+size(max_size_per_day))
        if self.max_amount_of_files == 0:
            print("Max. amount of files per day: Unlimited")
        else:
            print("Max. amount of files per day: "+str(max_amount_of_files))


    def upload(self, file):
        print("Unimplemented")

    def canUpload(self, file):
        newsize = os.path.getsize(file)
        current = calendar.timegm(time.gmtime())
        if dictlogging.lastts == current:
            print("Wait a second...")
            if dictlogging.nextToBeRemoved != None:
                self.nextsleep = 1
            return False
        if len(dictlogging.log) >= self.max_amount_of_files and self.max_amount_of_files > 0:
            print("Too many files uploaded in the last 24h")
            if dictlogging.nextToBeRemoved != None:
                self.nextsleep = current - dictlogging.nextToBeRemoved + 1
            return False
        if dictlogging.totalsize + newsize > self.max_size_per_day and self.max_size_per_day > 0:
            print("Space limit of hoster reached.")
            if dictlogging.nextToBeRemoved == None:
                print("!!!Critical!!! Your file is too large for this hoster!")
            else:
                self.nextsleep = (24*60*60) - (current - dictlogging.nextToBeRemoved) + 1
            return False
        print(" Uploads in the last 24h: "+str(len(dictlogging.log))+"/"+str(self.max_amount_of_files))
        print(" Upload size in the last 24h: "+size(dictlogging.totalsize)+"/"+size(self.max_size_per_day))
        return True

    def sleep(self):
        print("Sleep for "+str(self.nextsleep)+" seconds. Then the uploader should be ready for the next file.")
        time.sleep(self.nextsleep)
        self.nextsleep = 30