#Andy sixx
import os, calendar, time

def saveDictToFile(dic,file):
    f = open(file,'w')
    f.write(str(dic))
    f.close()

def loadDictFromFile(file):
    if os.path.exists(file) == False:
        return {}
    f = open(file,'r')
    data=f.read()
    f.close()
    return eval(data)

log = {}
log = loadDictFromFile("log.txt")

totalsize = 0
lastts = 0
nextToBeRemoved = None


def saveLog():
    saveDictToFile(log, "log.txt")


def registerUpload(file):
    global log
    global ts
    ts = calendar.timegm(time.gmtime())
    log[ts] = os.path.getsize(file)
    saveLog()

def updateLog():
    global totalsize
    global log
    global lastts
    global nextToBeRemoved
    totalsize = 0
    nextToBeRemoved = None
    
    current = calendar.timegm(time.gmtime())

    toRem = []

    for ts in log:
        if ts > lastts:
            lastts = ts
        if current - ts > 3600 * 24:
            toRem.append(ts)
        else:
            totalsize = totalsize + log[ts]
            if nextToBeRemoved == None or ts < nextToBeRemoved:
                nextToBeRemoved = ts

    for r in toRem:
        del log[r]

    if len(toRem) != 0:
        saveLog()