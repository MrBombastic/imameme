FROM ubuntu:18.04
MAINTAINER MrBombastic igotnomail@pooo.in.looo.mooo.com

ENV VIRTUAL_ENV=/opt/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN mkdir /unnamed
RUN mkdir /renamed

RUN apt-get update -qq
RUN apt-cache depends python-pycurl
RUN apt-cache depends python3-pycurl
RUN apt-get install libcurl4-openssl-dev libssl-dev -y

RUN apt-get install git python3 virtualenv -y
RUN python3 -m virtualenv --python=/usr/bin/python3 $VIRTUAL_ENV;
RUN git clone https://gitlab.com/MrBombastic/imameme.git

RUN cd imameme && \
    cp configs/docker_config.py ./config.py && \
    pip install -U setuptools

RUN apt-get install python3 python-dev python3-dev \
     build-essential libssl-dev libffi-dev \
     libxml2-dev libxslt1-dev zlib1g-dev \
     python-pip -y

RUN pip uninstall pycurl

RUN cd imameme && pip install -r requirements3.txt;

CMD ["python","-u","/imameme/main.py"]
