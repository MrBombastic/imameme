import requests, json, config

#Just a simple piece of code to talk to the MRISA instance

url = config.mrisa_url + "/search"

def clearGuess(guess):
    guess = guess.replace("/"," ")
    return guess

def getGuess(img_url):
    data = {
        "image_url":img_url
    }

    headers = {'Content-type': 'application/json'}
    r = requests.post(url, headers=headers, data=json.dumps(data))

    guess = r.json()['best_guess']

    guess = clearGuess(guess)
    
    return guess
