# ImaMeme

**IMA**ge and **MEME** (re-)namer

## Usecase

![Images before and after they have been renamed](unnamed-to-renamed.png)

**ImaMeme** is able to rename up to 100 of your images per day with the help of Google reverse image search. This is just basically a script that goes through one folder, finds all images, uploads them to a temporary image hoster (or uses your own webserver), conducts a reverse image search using MRISA and uses googles guess as the name. Additionally, this script tries to evaluate the quality of the existing name. In the end there are 4 different cases:

1. The old name is not suitable and google returns a guess: In this case googles guess replaces the old name. `1337148869.jpg` might be renamed to `chad.jpg`
2. The old name is suitable and google returns a guess: In this case the new name is: `<old_name> (<google_guess>).<file_extension>`. So maybe it becomes `chad (chad smirks).jpg`.
3. The old name is suitable and google does not return a guess: In this case the old name is kept.
4. The old name is not suitable and google does not return a guess: In this case the file just receives `___` as a prefix. Sorting the target folder by name should list these files at the top, unless you have some very uncomming file names.

## Installation:

### Default:

#### Prerequisites:

1. You need a working installation of python (2 or 3)
2. The system was designed for use on linux. It should run fine on WSL too. In runs on windows too, however you should expect possible complications with pycurl. If necessary you might have to disable ssl in the config to make it work (try it with first).
3. This script requires a running instance of MRISA. Make sure to change the port to whatever you select in the config.py (default:5000). You can find it here: https://github.com/vivithemage/mrisa/
4. Obviously you need a working internet connection

#### Installation:

1. Just clone or download and unpack this repository.
2. Run `pip install -r requirements2.txt` or `pip install -r requirements3.txt` depending on your python version.


##### Pycurl-Troubleshooting

Be aware that some some installations have problems with pycurl.

Linux (Ubuntu/Debian) users might get some help from this: https://stackoverflow.com/questions/37669428/error-in-installation-pycurl-7-19-0

Sometimes these commands might help:
```
apt-get install libcurl4-openssl-dev libssl-dev -y
apt-get install python3 python-dev python3-dev build-essential libssl-dev libffi-dev libxml2-dev libxslt1-dev zlib1g-dev python-pip -y
```

For Windows users i would recommend downloading pycurl here: https://dl.bintray.com/pycurl/pycurl/
I somehow managed to get pycurl to work on one of my windows installations but don't ask me how.

If you have a ssl problem (on windows) you might have to disable ssl in the config.

### Docker:

You need at least 1 GB of free space.

1. Download the Dockerfile.
2. Build the image with: `docker build --network=host -t imameme:latest .`
3. Create a container with: `docker create --name=imameme -v /path/to/source:/unnamed -v /path/to/target:/renamed --restart unless-stopped imameme`

Make sure to change the paths to your source and target folder in the docker create 


## Configuration:

You can make all needed adjustments to the `config.py`. Make sure to select the correct paths for the unnamed and renamed image folders.

### Desktop use:

If you plan on using this on a device that is not always on i would recommend using lower `min_sleep_after_rename` and `max_sleep_after_rename` values but leave `max_amount_of_requests_per_24h` at 100. Then just start MRISA and this script automatically in the background at system start. The script will make requests but will not go over the 100 request limit. If you are using the device for a couple of hours it will reach the limit, giving you the same result as a 24/7 server (for that day).

Perhaps the following (incomplete!) `config.py` lines will do the trick:
```python
min_sleep_after_rename = 60
max_sleep_after_rename = 300
max_amount_of_requests_per_24h = 100
```

## Execution:

### Default:

1. Run a MRISA instance
2. Execute `python main.py` from inside the repository

I would recommend you to use screens to run this script in the background.

#### Linux:

Start with screen:
`screen -S ImaMeme python /path/to/repository/main.py`

Start with screen and generate a logfile:
`screen -L imameme.log -S ImaMeme python /path/to/repository/main.py`

Alternatively you could start the script automatically with `crontab -e`:

Just add `@reboot python /path/to/repository/main.py`.

I would recommend combining both, so starting a screen at startup using a cronjob.

### Docker:

Use `docker run imameme` to run the container that you have already created.

## Planned/to-do:

fix execution of command after image rename ✓

replace linux commands to support other operating systems ✓

log work to enable intelligent decision making ✓

impose limits for uploader (100MB/day for uguu_se) ✓

add desktop mode for people who have no server (dynamic sleep times based on upload history) ✓

video support (will probably require ffmpeg, issue with os compability...)

add function to collect anonymous user data if user agrees (i love data hoarding) ✓

create docker container for easy integration

## Notes:

Google imposes a limit to requests per user. For this reason this script makes less than 100 requests per day if it keeps running. The script is designed for use on an always-on server. It might make sense for you to change the worldlist text file if you are using a different language.

## Footnote:

The idea came from an anon on /g/ in 2019H1.

I have used this script from August 2019 to December 2019 to rename 13k Images.

Feel free to ask any (related, non-moronic) questions and report any bug you find.

## Other image hosters:

If you know any better temporary hosters like uguu.se that could be used for this project please let me know. If you are able to implement the required class and functions (similar to `uguu_se.py`) i would gladly accept a merge request.