from uguu_se import uguu_se

#This script uses its own root directory as the starting point for any relative path(!) Please make sure to remember this should you be planning on using custom paths.

#If you want to run the script you must set active to True. Please check the other settings first before you do this!
active = True

#Do not end the url with a slash. The default port of MRISA is 5000.
mrisa_url = "http://127.0.0.1:5000"

#Configure the used uploader, must include an upload function. Specify 'None' if you want to use a webserver on your machine. The advantage of using your own webserver is that the file is deleted right after google has formulated a guess. Also no third party is required (as opposed to a third party hoster such as uguu_se).
uploader=uguu_se()

#Webserver url, only used when uploader=None is configured. Used path is <url>/<image>. Make sure the domain directory ends with a slash.
webserver_url = "https://mydomain.com/imameme/"
webserver_path = "/var/www/mydomain/imameme/"

#Language file
wordlist = "wordlists/english+german+latin+music+names.txt"

#Image names that should be ignored when determining the quality of the existing name
ignorenames = {"maxresdefault", "image", "screenshot", "download"}

#Source directory, contains all images that should be renamed:
source = "unnamed/"

#Target directory, contains all images that have been renamed:
target = "renamed/"

#Command to be executed after each time an image is renamed. Parameter 1 given to this is the original path, Parameter 2 is the new path
#make sure to specify #!/bin/bash or something similar
#Default: None
after_rename_script = None

#Image file extensions. List of the file extensions that should be used.
image_file_extensions = {'.jpg','.jpeg','.png','.gif'}

#Video file extensions. List of the file extensions of videos that should be used. These are converted to gifs. Low rate of success. Also requires ffmpeg. Currently not implemented.
video_file_extensions = {}
#video_file_extensions = {'.webm','.mp4','.mkv','.avi'}

#Sleep for at least this amount of seconds, Default: 900 means min 3 renames per hour -> 72 per day
min_sleep_after_rename = 60

#Sleep for at most this amount of seconds. Default: 1200 means max 4 renames per hour -> 96 per day. More than 100 requests per day may cause problems.
max_sleep_after_rename = 300

#Max amount of renames per 24h. When using low sleep times (above) but the 100 requests limit you might be able to use this script on a device that isn't always on to its full extent.
max_amount_of_requests_per_24h = 100

#Amount of seconds to sleep when no compatible file is found in the source directory
sleep_when_no_file_found = 3600

#Use SSL whenever possible, otherwise you are in theory prone to man-in-the-middle attacks. Should you however encounter a problem with pycurl such as: "pycurl.error: (23, 'Failed writing body (0 != 48)')" you might want to disable ssl
use_ssl = 1

#Determines if the program should continue or stop after no valid source files are found. True means it will wait for <sleep_when_no_file_found> seconds and look for new files. False means it just stops.
keep_program_running = True

